import networkx as nx
import pandas as pd
import json
import matplotlib.pyplot as plt
import pprint
from collections import Counter
pp = pprint.PrettyPrinter()

from sink import Sink, SinkAnalysis
sink = Sink("stats")
l_sink = SinkAnalysis(sink.G, sink.reversed_G, sink.pagerank)
l_sink.convert_pagerank()

# approach 1. 
# start with one tag. 

# this can be anything. pca. 

# we look for questions with atleast one of pca and one new term or just a new term.

# your need to include only the questions which are out coming out the nodes in the links list and not all. shit. should have included that.  let me do it now

# add the question which has only pca but has the highest score

# we also need to include terms from math and datascience and stackoverflow. and then also a way to optimize and add reward and penalty sort of thing
# so that the agent can all on its own.
tags_hash = {}
start = 'pca'
tags_hash[start] = True
seen = {}
link = []
linear_algebra_pca = []
for node in l_sink.G.nodes():
  if 'pca' in l_sink.G.node[node]['attr_dict']['tags']  and len(l_sink.G.node[node]['attr_dict']['tags']) == 2:
   linear_algebra_pca.append(node)


from collections import Counter
c = Counter()
for node in linear_algebra_pca:
	print(l_sink.G.node[node]['attr_dict']['tags'])
	c += Counter(l_sink.G.node[node]['attr_dict']['tags'])

# now check all the questions.
max_score = -float("inf")
tag_select = None
for tag in c:
	if tag != start:
		score = 0
		for node in l_sink.G.nodes():
			tags = l_sink.G.node[node]['attr_dict']['tags'] 
			if start in tags and tag in tags:
				score += l_sink.G.node[node]['attr_dict']['score'] 
		if score > max_score:
			tag_select = tag
			max_score = score
		print(tag, score, tag_select, max_score)


pca_max = max(linear_algebra_pca, key = lambda x: l_sink.G.node[x]['attr_dict']['score'])
print(pca_max, l_sink.G.node[pca_max]['attr_dict']['score'])
link.append(pca_max)
steps = 2
current_step = 0
score = 0
while current_step < steps:
	nodes = []
	to_select = []
	for n in link:
		to_select.extend([x[0] for x in l_sink.G.in_edges(n)])
	for node in to_select:
		if node not in seen:
			count = 0
			# should have only one new term
			found = 0
			for tag in l_sink.G.node[node]['attr_dict']['tags']:
				if tag not in tags_hash: # if the tag is not present in the tags_hash
					count += 1
					for tag1 in l_sink.G.node[node]['attr_dict']['tags']: # one of other tags is present then include it.
						if tag1 != tag and tag1 in tags_hash:
							found = 1
				if count > 1:
					found = 2
					break
			if count == 1 and found == 1:
				nodes.append(node)
			else:
				continue
	if len(nodes) > 0:
		# find the question with the highest score
		selected = max(nodes, key = lambda x: l_sink.G.node[x]['attr_dict']['score'])
		print(selected, l_sink.G.node[selected]['attr_dict'])
		score += l_sink.G.node[selected]['attr_dict']['score']
		seen[selected] = True		
		for tag in l_sink.G.node[selected]['attr_dict']['tags']:
			if tag not in tags_hash:
				tags_hash[tag] = True
		link.append(selected)
	current_step += 1

print(score)



