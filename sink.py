import networkx as nx
import pandas as pd
import json
import matplotlib.pyplot as plt
from collections import Counter
import sink
import json
import math
from collections import Counter

class Sink:
  def __init__(self, db):
    self.G = nx.read_gpickle(db + "_posts_modified.gpickle")
    self.reversed_G = nx.read_gpickle(db + "_reversed_posts_modified.gpickle")
    self.pagerank = json.load(open(db + "_pagerank.json"))

    
class SinkAnalysis:
  def __init__(self,G,reversed_G,pagerank):
    self.G = G
    self.reversed_G = reversed_G
    self.pagerank = pagerank
    self.convert_pagerank()
    
  def convert_pagerank(self):
    for key in self.pagerank.keys():
      if type(key) is str:
        try:
          self.pagerank[int(key)] = self.pagerank[key]
        except:
          pass
        del self.pagerank[key]
  # each question thus could be a presentative of the question that follows it only. and nothing farther. we have found that the weight of the edge between
  # two drect ones are the strongest. and not the ones that are two edges away.
  # but by just knowing the tags it would be difficult to find the order of the questions.

  # suppose i do get the pca tree. 
  def find_tags(node):
    graph = self.create_tree(node)
    distance = {node: 1}
    queue = [node]
    reversed_graph = graph.reverse()
    while queue:
      node = queue.pop(0)
      sorted_first_level = ([x[1] for x in reversed_graph.out_edges(node)])
      for question in sorted_first_level:
        distance[question] = distance[node] + 1

    node_tag_counter = Counter(l_sink.G.node[node]['attr_dict']['tags'])
    for node in distance:
      tag_counter = Counter(l_sink.G.node[node]['attr_dict']['tags'])
      for tag in tag_counter:
        tag_counter[tag] = tag_counter[tag]/(distance[node]**2)
      node_tag_counter = node_tag_counter + tag_counter
    

    s = sum(node_tag_counter.values())
    for tag in node_tag_counter:
      node_tag_counter[tag] /= s
    return node_tag_counter



  def create_tree(self, parent):
    paths = []
    queue = [parent]
    distance = {}
    parents = {}
    distance[parent] = {'d': 0, 'parent': None}
    visited = {}
    while queue:
      node = queue.pop(0)
      visited[node] = True
      for neighbour in [x[1] for x in self.G.out_edges(node)]:
        if neighbour not in visited:
          queue.append(neighbour)
          distance[neighbour] = {'d': distance[node]['d'] + 1, 'parent': [node]}
          parents[node] = True
    new_G = nx.DiGraph()
    for node in distance:
      if node not in parents:
        link = nx.shortest_path(self.G, source=parent, target=node)
        link.reverse()
        prev = link[0]
        n = None
        for point in link[1:]:
          if not new_G.has_node(prev):
            dict_a = {'score': self.G.node[prev]['attr_dict']['score'], "tags": Counter(self.G.node[prev]['attr_dict']['tags'])}
            new_G.add_node(prev, attr_dict=dict_a)
            n = point
          if not new_G.has_node(point):
            dict_b = {'score': self.G.node[point]['attr_dict']['score'], "tags": Counter(self.G.node[point]['attr_dict']['tags'])}   
            new_G.add_node(point, attr_dict=dict_b)
            n = point
          if not new_G.has_edge(prev, point):
            d = new_G.node[prev]['attr_dict']['tags'] + new_G.node[point]['attr_dict']['tags']
            new_G.node[point]['attr_dict']['score'] += new_G.node[prev]['attr_dict']['score']
            new_G.node[point]['attr_dict']['tags'] = d
            new_G.add_edge(prev, point)
          else:
            d = new_G.node[n]['attr_dict']['tags'] + new_G.node[point]['attr_dict']['tags']
            new_G.node[n]['attr_dict']['score'] += new_G.node[prev]['attr_dict']['score']
            new_G.node[point]['attr_dict']['tags'] = d
          prev = point           
    return new_G

# the sinks method doesnt play a lot of role. The score and the tags are the most important. Linking analysis is also important which we got to study
  def find_sinks(self, start):
    sinks = []
    if self.G.out_degree(start) == 0:
      sinks.append((start, 0))
    queue = [start]
    visited = {}  
    d = {}
    d[start] = 0
    while queue:
      node = queue.pop(0)
      visited[node] = True
      for neighbour in [x[1] for x in self.G.out_edges(node)]:
        if neighbour not in visited:
          d[neighbour] = d[node] + 1
          if len(self.G.out_edges(neighbour)) == 0:          
            sinks.append((neighbour, d[neighbour]))
          else:
            queue.append(neighbour)       
    return sinks



  def find_sinks_from_root(self, start):
    sinks_graph = nx.DiGraph()
    queue = [start]
    visited = {}
    sinks = [start]
    while queue:
      node = queue.pop(0)
      visited[node] = True
      for neighbour in [x[1] for x in self.G.out_edges(node)]:
        if neighbour not in visited:
          if len(self.G.out_edges(neighbour)) == 0:
            sinks.append(neighbour)
            sinks_graph.add_edge(node, neighbour)
          else:
            queue.append(neighbour)
    return sinks, sinks_graph

  # need a way to find which sink to ignore. Not all sinks are important for my case. 
  def find_other_sinks_from_one_sink(self, sink):  
    parent_sink = sink
    sink_tree = nx.DiGraph()
    all_sinks = self.find_sinks(sink)
    sinks = [x[0] for x in all_sinks]
    sinks_visited = {}
    parent = {}
    while sinks:
      sink = sinks.pop(0)
      sinks_visited[sink] = True
      queue = [x[1] for x in self.reversed_G.out_edges(sink)]
      neighbours_visited = {}
      while queue:
        neighbour = queue.pop(0)
        if neighbour not in neighbours_visited:
          found_sinks = [x[0] for x in self.find_sinks(neighbour)]
          neighbours_visited[neighbour] = True
          queue.extend([x[1] for x in self.reversed_G.out_edges(neighbour)])
          for new_sink in found_sinks:
            if new_sink != sink and new_sink not in sinks_visited:
              # should add only those sinks which are heavy pagerank wise. dont want to add sinks which are of no use anyhow. 
              if self.pagerank[new_sink] >= self.pagerank[parent_sink]:
                sink_tree.add_edge(sink, new_sink)
                sinks.append(new_sink)
                sinks_visited[new_sink] = True
    return sink_tree

  def show_graph(self, e):
    nx.draw(e, with_labels=True)
    plt.show()

  def total_sinks(self):
    total_sinks = []
    for node in self.G.nodes():
      if len(self.G.out_edges(node)) == 0:
        total_sinks.append(node)
    sorted_sinks = sorted(total_sinks, key=lambda x: p[x], reverse = True)
    tree = nx.DiGraph()
    for sink in sorted_sinks[:10]:
      create_tree(tree, sink)


  def similarity(self, question1, question2):
    assert(self.G.out_degree(question1) == 0)
    assert(self.G.out_degree(question2) == 0)
    return self.counter_cosine_similarity(question1, question2)

  def counter_cosine_similarity(self, question1, question2):
    try:
      import math
      node = question1
      graph = self.create_tree(node)
      c1 = graph.node[node]['attr_dict']['tags']
      node = question2
      graph = self.create_tree(node)
      c2 = graph.node[node]['attr_dict']['tags']
      terms = set(c1).union(c2)
      dotprod = sum(c1.get(k, 0) * c2.get(k, 0) for k in terms)
      magA = math.sqrt(sum(c1.get(k, 0)**2 for k in terms))
      magB = math.sqrt(sum(c2.get(k, 0)**2 for k in terms))
      return dotprod / (magA * magB)
    except:
      return 0


  def cal(self, node):
    print(node)
    pp.pprint(self.find_sinks(node))
    paths = self.create_tree(node)
    pp.pprint(paths)
    graph = self.create_graph(node)
    pp.pprint(graph.node[node])


