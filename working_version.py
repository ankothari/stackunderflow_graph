import networkx as nx
import pandas as pd
import json
import matplotlib.pyplot as plt
import pprint
from collections import Counter
pp = pprint.PrettyPrinter()

from sink import Sink, SinkAnalysis
sink = Sink("stats")
l_sink = SinkAnalysis(sink.G, sink.reversed_G, sink.pagerank)
l_sink.convert_pagerank()



linear_algebra_pca_sorted = sorted(linear_algebra_pca, key=lambda x: l_sink.G.node[x]['attr_dict']['score'], reverse = True)

for node in linear_algebra_pca_sorted:
  node = 329521
  print()
  print(node)
  graph = l_sink.create_tree(node)
  queue = [node]
  distance = {}
  parents = {}
  distance[node] = {'d': 0, 'parent': None}
  tags = {}
  level = {}
  level[0] = [node]
  tags_distance = {}
  tags_distance[0] = Counter(l_sink.G.node[node]['attr_dict']['tags'])
  visited = {}
  while queue:
    node = queue.pop(0)
    visited[node] = True
    for neighbour in [x[1] for x in l_sink.G.out_edges(node)]:
      if neighbour not in visited:
        queue.append(neighbour)
        distance[neighbour] = {'d': distance[node]['d'] + 1, 'parent': [node]}
        if distance[node]['d'] + 1 in tags_distance:
          tags_distance[distance[node]['d'] + 1] = Counter(tags_distance[distance[node]['d'] + 1]) + Counter(l_sink.G.node[neighbour]['attr_dict']['tags'])
        else:
          tags_distance[distance[node]['d'] + 1] = Counter(l_sink.G.node[neighbour]['attr_dict']['tags']))
        if distance[node]['d'] + 1 in level:
          level[distance[node]['d'] + 1].append(neighbour)
        else:
          level[distance[node]['d'] + 1] = [neighbour]
        parents[node] = True
  # for node in nx.dag_longest_path(graph):
  #   print(node, l_sink.G.node[node])

#nodes = []
#for node in l_sink.G.nodes():
#  if l_sink.G.in_degree(node) == 0:
#   nodes.append(node)
  

linear_algebra_pca = []
for node in l_sink.G.nodes():
  if 'pca' in l_sink.G.node[node]['attr_dict']['tags']:
   linear_algebra_pca.append(node)
nodes_sorted = sorted(linear_algebra_pca, key=lambda x: l_sink.pagerank[x], reverse = True)

# There is a question which has a very high pagerank. and many questions actually point to it.


# this is lame. also this question is not of much importance till we know eactly what the question is mode of

# linear_algebra_pca = []
# for node in l_sink.G.nodes():
#   if 'logistic' in l_sink.G.node[node]['attr_dict']['tags']:
#    linear_algebra_pca.append(node)
# nodes_sorted = sorted(linear_algebra_pca, key=lambda x: l_sink.G.out_degree(x), reverse = True)


linear_algebra_pca = []
for node in l_sink.G.nodes():
  if 'svd' in l_sink.G.node[node]['attr_dict']['tags'] and len(l_sink.G.node[node]['attr_dict']['tags']) == 1:
   linear_algebra_pca.append(node)
nodes_sorted = sorted(linear_algebra_pca, key=lambda x: l_sink.G.in_degree(x), reverse = True)
visited = {}
for node in nodes_sorted:
    for in_node in [x[1] for x in l_sink.G.out_edges(node)]:
        if in_node not in visited:
            #visited[in_node] = True
            print(node, in_node, l_sink.G.node[in_node]['attr_dict'])

# i am leaning more on using association rule mining, or a decision tree, because given two terms 
# which item occurs next given that the item doesnt occur a lot with other items. ok, we are already
# given the rules. we jus thave to find the term which gets a lift. or a metric like support, 
# confidence of lift. 
    
# so given a frequent ite,s we have to come up with association rules.
d = {}
for node in nodes_sorted:
    d[node] = l_sink.G.out_degree(node)
c = Counter()
visited = {}
for node in nodes_sorted:
node = 20523
# i think from here I can create an association rule mining which says that if a question 
# on something has been asked then what concepts will be present to explain the most. 


#http://blog.kaggle.com/2017/09/21/instacart-market-basket-analysis-winners-interview-2nd-place-kazuki-onodera/


for out_node in  l_sink.G.in_edges(node):
    if out_node not in visited:
        visited[out_node] = True
        c += Counter(l_sink.G.node[out_node[0]]['attr_dict']['tags']) 
sorted(c, key=c.get) 
# based on what questions link , we can find what the following question is trying to explain.
# because there are many questions which refer here, and we can use their tags ot find whicht ag would be the most useful. 
# and also if a question is going to be explained my many questions then we can find what the 
# question is made of. 


pp.pprint([(l_sink.G.node[node[0]]['attr_dict'], l_sink.G.in_degree(node[0]), node[0]) for node in  l_sink.G.in_edges(20523)])
# can we show how the percentage of the concepts cary as we transverse thru the longest path ?

# node = 226905
# out of all the questions in pca which has an edge. find what topics are required to explain
# the question. 
# graph = l_sink.create_tree(node)
# pp.pprint(graph.node[node])
# nx.dag_longest_path(graph)

# queue = [2691]
# reversed_graph = graph.reverse()
# while queue:
#   node = queue.pop(0)
#   sorted_first_level = ([x[1] for x in reversed_graph.out_edges(node)])
#   for question in sorted_first_level:
#     try: 
#       print(question, l_sink.counter_cosine_similarity(node, question))
#       queue.append(question)
#     except:
#       pass

# node = 21131
# queue = [node]
# visited = {}
# while queue:
#   node = queue.pop(0)
#   visited[node] = True
#   distance = {node: 1}
#   print(node)
#   graph = l_sink.create_tree(node)
#   if len(graph.nodes()) > 0:
#     reversed_graph = graph.reverse()
#     sorted_first_level = ([x[1] for x in reversed_graph.out_edges(node)])
#     for question in sorted_first_level:
#       if question not in visited:
#         distance[question] = distance[node] + 1
#         queue.append(question)
#     node_tag_counter = Counter(l_sink.G.node[node]['attr_dict']['tags'])
#     for node in distance:
#       tag_counter = Counter(l_sink.G.node[node]['attr_dict']['tags'])
#       for tag in tag_counter:
#         tag_counter[tag] = tag_counter[tag]/(distance[node]**2)
#       node_tag_counter = node_tag_counter + tag_counter
#     for tag in node_tag_counter:
#       count = 0
#       t_c = 0
#       for node in l_sink.G.nodes():
#         t_c += 1
#         if tag in l_sink.G.node[node]['attr_dict']['tags']:
#           count += 1
#       node_tag_counter[tag] *=np.log(t_c/count)
#     s = sum(node_tag_counter.values())
#     for tag in node_tag_counter:
#       node_tag_counter[tag] /= s  
#     print(node_tag_counter)

# 


