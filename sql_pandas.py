from google.cloud import bigquery
import operator
import networkx as nx
from nltk.util import ngrams
from nltk import FreqDist
from gensim.models import Word2Vec
import os
from sklearn import metrics
from scipy.spatial.distance import cdist
import numpy as np
from sklearn.cluster import KMeans
import csv 
import sys #used for passing in the argument
import pickle
from bs4 import BeautifulSoup
import pandas
from nltk.stem.porter import *
from nltk.tokenize import RegexpTokenizer
from google.cloud import bigquery
import pandas as pd
from sqlalchemy import create_engine


stemmer = PorterStemmer()
tokenizer = RegexpTokenizer(r'[a-zA-Z][^\s]*\b')

import pandas as pd
from sqlalchemy import create_engine


sql = "SELECT id, tags, score from posts" 
engine = create_engine('postgresql://localhost:5432/stackoverflow')
df = pd.read_sql_query(sql, engine)


sql = "SELECT tagname from tags" 
engine = create_engine('postgresql://localhost:5432/stackoverflow')
df_tags = pd.read_sql_query(sql, engine)

e = nx.ego_graph(reversed_G, 133610, 1)

def clean(question):
  question = question.lower()
  question_text = BeautifulSoup(question, "lxml").get_text()
  tokens = tokenizer.tokenize(question_text)
  filtered_sentence = [w for w in tokens if not w in stop_words]
  
  filtered_stems = [stemmer.stem(plural) for plural in filtered_sentence]
  return filtered_stems

engine = create_engine('postgresql://localhost:5432/stackoverflow')
stop_words = ['i', 'me', 'my', 'myself', 'we', 'our', 'ours', 'ourselves', 'you', "you're", "you've", "you'll", "you'd", 'your', 'yours', 'yourself', 'yourselves', 'he', 'him', 'his', 'himself', 'she', "she's", 'her', 'hers', 'herself', 'it', "it's", 'its', 'itself', 'they', 'them', 'their', 'theirs', 'themselves', 'what', 'which', 'who', 'whom', 'this', 'that', "that'll", 'these', 'those', 'am', 'is', 'are', 'was', 'were', 'be', 'been', 'being', 'have', 'has', 'had', 'having', 'do', 'does', 'did', 'doing', 'a', 'an', 'the', 'and', 'but', 'if', 'or', 'because', 'as', 'until', 'while', 'of', 'at', 'by', 'for', 'with', 'about', 'against', 'between', 'into', 'through', 'during', 'before', 'after', 'above', 'below', 'to', 'from', 'up', 'down', 'in', 'out', 'on', 'off', 'over', 'under', 'again', 'further', 'then', 'once', 'here', 'there', 'when', 'where', 'why', 'how', 'all', 'any', 'both', 'each', 'few', 'more', 'most', 'other', 'some', 'such', 'no', 'nor', 'not', 'only', 'own', 'same', 'so', 'than', 'too', 'very', 's', 't', 'can', 'will', 'just', 'don', "don't", 'should', "should've", 'now', 'd', 'll', 'm', 'o', 're', 've', 'y', 'ain', 'aren', "aren't", 'couldn', "couldn't", 'didn', "didn't", 'doesn', "doesn't", 'hadn', "hadn't", 'hasn', "hasn't", 'haven', "haven't", 'isn', "isn't", 'ma', 'mightn', "mightn't", 'mustn', "mustn't", 'needn', "needn't", 'shan', "shan't", 'shouldn', "shouldn't", 'wasn', "wasn't", 'weren', "weren't", 'won', "won't", 'wouldn', "wouldn't"]

chunk_size = 10000
offset = 0
while True:
  sql = "SELECT id, body FROM posts limit %d offset %d" % (chunk_size,offset) 
  df_piece = pd.read_sql_query(sql, engine, chunksize=5000)

  for chunk in df_piece:
    chunk['clean_body'] = chunk['body'].apply(clean)
    chunk.to_sql(name='clean_posts', con=engine, if_exists = 'append', index=False)

https://console.cloud.google.com/compute/

G = nx.read_gpickle("posts_modified.gpickle")
reversed_G = G.reverse()
nx.write_gpickle(reversed_G, "reversed_posts_modified.gpickle")


#store the score and tags, and title in the node. and nothing in the edges.
import networkx as nx
import pandas as pd
from sqlalchemy import create_engine
import re
sql = "SELECT postid, relatedpostid, a.score as a_score, b.score as b_score, a.tags as a_tags, b.tags as b_tags FROM postlinks INNER JOIN posts AS a ON a.id = postlinks.postid INNER JOIN posts AS b ON b.id = postlinks.relatedpostid" 
engine = create_engine('postgresql://localhost:5432/math_stackoverflow')
df = pd.read_sql_query(sql, engine)
df.columns = ["postid", "relatedpostid", "ascore", "bscore", "atags", "btags"]

G=nx.DiGraph()
for row in df.iterrows():
  try:
    tags = re.findall(r"<([^<^>]*)>*", row[1][4])
    dict_a = {'score': row[1][2], "tags": tags}
    tags = re.findall(r"<([^<^>]*)>*", row[1][5])
    dict_b = {'score': row[1][3], "tags": tags}
    G.add_node(row[1]['postid'], attr_dict=dict_a)
    G.add_node(row[1]['relatedpostid'], attr_dict=dict_b)
    G.add_edge(row[1]['postid'], row[1]['relatedpostid'])
  except:
    print(row)


nx.write_gpickle(G, "math_posts_modified.gpickle")

reversed_G = G.reverse()
nx.write_gpickle(reversed_G, "math_reversed_posts_modified.gpickle")

p = nx.pagerank(G)

import json

with open('math_pagerank.json', 'w') as fp:
    json.dump(p, fp)




for comp in nx.connected_components(G):
  print(comp)







for node in G.nodes():
  try:
    tags = re.findall(r"<([^<^>]*)>*", G.node[node]['attr_dict']['tags'])
    d = G.node[node]['attr_dict']
    counter = Counter(tags)
    d['tags'] = counter
    G.node[node]['attr_dict'] = d
  except:
    print(node, G.node[node]['attr_dict']['tags'])

for node in G.nodes():
  try:
    tags = G.node[node]['attr_dict']['tags']
    d = G.node[node]['attr_dict']
    counter = Counter(tags)
    d['tags'] = counter
    G.node[node]['attr_dict'] = d
  except:
    print(node, G.node[node]['attr_dict']['tags'])


nx.write_gpickle(G, "_maths_posts_modified_counter.gpickle") 


# above one is all fine, we just did an analysis. 
# starting a node we need to decide which path to take. And this path depends on the user's past history. What is the main goal? Given a node can we build a graph only from that 
# point towards the sink and then transverse the graph? 
# and then find the  shorted distance between itself and the sinks. 

# depending on this score we then decide which question to show him next. 
# for the start just build a recommender engine. after that decide which question to show to make him learn new things. I think these both are equivalent. 
The new question should be such that he learns something new. Given his score is less, you take him to a new concept which is present in the next group. You are helping him
navigate to other groups to looking at an answer since he isnt able to find an answer and unable to understand something. 
So given a question help the user navigate the various questions so that u can help him learn the question. so that u have an tree of concepts and the user is simply learning
by reading them So this would be my first goal. Given a question show to the usr various questions/concepts that can help the user understand the current question. once this is
done later u can figure out how to make the user ansswer the question. So given a question, show which question to take next, or which question to read for understandng that 
answer. Bascially you just show questions from one group to another either forward or backward. And this what I have been trying to build. 

given a sink, go back,  to all the leaf nodes. get these leaf nodes. then start moving forward. 

but then rank the number of times a sink appears. and then rank them. see this pattern first. 

start from a sink. a well known sink or start it from git. it is a sink. But then we also need to find a question tha that hasnt been answered or the score is less 
using the stackoverflow api and show to the user to answer it. 


go reverse direction, find all the leaf nodes. starting each leaf node find the sinks and then rank them. karke dekh lete he yeh bhi.

We can get a sink connected to many sinks but then question is how do u give weight to the edges? Why am i doing this? Given a question, how do u decide which question might
of help to the user to ansswer the question. Currently given a question just show the questions/sinks which can help you look at the prevous cocnepts and also the next cocnepts. 
Once this is done then u can decide how to create the education/testing part. 

Given a question, and how a user has answered it, we need to take him to a different concept he needs to learn, and then what are the questions to show to him to take to understand that concept. 


Given a question find the sink.


BINGO. 

how do u decide which questions immediately before a question ? 

one step away from sink, find what do they connect to but at only one distance. 

questions that are one distance away also point to other places. 

all_simple_paths(G, source, target, cutoff=None)

# can i use frequent pattern mining for doing type of analysis? this way atleast we can find which question comes next if a question was selected and then suggest that question. 
but this wont tell me the concept dependencty graph.


given the user has answered a question or read something, find something else in relation. problem is how far up do u go? u want to find a question that is the most similar to the
sink question. need to find a different question at a distance of 1.


given a sink find all questions at a disnace of 1 from the sink node. but all those questions might not be good. they might be a small percentage of the sink question.

is there a question on top of these questions that are actually pointing the sink question? if yes then how many. 
find the question that acutally point to this sink that are poitning to the above question as well.

My first target is to find an order of questions to show. 

I have all the sinks which have no outgoing links. These are my end goals. My target is to move the user towards this question. I have to structure my question set in such
a manner. 


We only show sink questions in some order given a topic or tag. we dont take into cinsideration what the student already knows. this will come later. 


convert each question into a vector of tags and all weighted by the score of the question. this df will be huge.


Now each question has been given some tags. maybe the algorithm is wrong but atleast the idea is right. If one question points to another, then we just club the tags. 

A question is also related to the question it actually links to. 

Given a question, what questions do u suggest without knowing anything abt the user? and if u do happen to know abt the user how will your recommendations change? Is your goal to help the user to look for an answer or help the user answer a question so that he learns about it. recommendation part will come first, once this is working at a good pace then we will expand to the reinforcement part. 

Given any question, find out which question (sink) will explain the most about the question? And once you have identified this, how will you trace back the path which will help us to know if the user knows anything about the question. And how will you combine what the user knows, and what he doesnt know to form the path. At each point, the question will be dependent on what the user knows and what we are trying to judge the user for. We might want to judge the user on a couple of topics but then we will want to make it easier for him. So at one point, you know what the user understands, what you want him to learn and then pick a question that will help learn as much as possible. 
So given a question, first find out which question should be the next sink that will explain the most about this question. Given a question, it might not even follow a sink whereas simply a node which then follows another node. first, find all the sinks starting with the question. decide which question is the most important. then traceback and then decide the next sink question. 

use the tags of the question. 

I want to teach the user the djikstras algorithm. So how do u decide how to teach and in what order? We can obviosuly get the sink from all the questions that have
dijkstra in their tags. take mutex and semaphore for example, suppose the sink is finalized that i want the user to learn about. 
We figure a question to ask the user which is free of other concepts and carries the most weight in terms mutex and semaphore. Maybe this quesiton isnt linked by any other question
 or we ask the question that is one link away from this sink question but which represents more about mutex and less about something else. same with djiktra's. the father u go 
 away from the question, the more the questions becomes a combination of topics. we want a pure topic which isnt combined of other concepts. first of all among all the tags
 we need to pick the tag that is the most uncommon and expains dijkitra's the most.  



